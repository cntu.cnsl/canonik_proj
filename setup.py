import os
from setuptools import find_packages, setup
from glob import glob  # Simple and easy to understand than listdir and os.walk
from os.path import basename
from os.path import splitext


with open("README.md", "r") as fi:
    long_description = fi.read()

# Get the requirements path
reqs_path = os.path.join(os.path.dirname(__file__), 'src/requirements.txt')
with open(reqs_path, "r") as req_file:
    dependencies = req_file.readlines()

setup(
    name="u_packages",
    version="0.0.1",
    install_requires=dependencies,
    author="TuCN",
    description="A simple tool for collecting Ubuntu packages",
    long_description=long_description,
    packages=find_packages("src"),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*py')],
    entry_points={
      "console_scripts": [
          'u-packages=u_packages.app:main',
      ],
      "u_packages.commands": [
          'help=u_packages.commands:HelpCommand',
          'info=u_packages.commands:InfoCommand',
      ]
    },
)
