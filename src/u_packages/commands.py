class HelpCommand():
    def run(self, *args):
        print("test")

    @staticmethod
    def describe():
        return "List all available commands, provide more detail about specific command"


class InfoCommand():
    def run(self, *args):
        """ Get mutiple packages information """
        for package_name in args:
            print(package_name)

    @staticmethod
    def describe():
        return "Get information from one or multiple packages"
