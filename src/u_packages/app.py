import sys
from sys import argv


from u_packages.commands import HelpCommand, InfoCommand

registered_commands = {}  # Type Dict[Text, Type[UpackageCommand]]

registered_commands.update({
    'help': HelpCommand,
    'info': InfoCommand,
})


def main():
    """ Command-line interface for u_packages

    Loads the command lines name and parameters from :py:data:`argv`
    """
    command_type = HelpCommand

    if (len(argv) > 1):
        command_name = argv[1]
        command_type = registered_commands.get(command_name, None)
        if command_type is not None:
            # Run help command
            command_type.run(*argv[1:])
            exit(0)
    print("Ubuntu Packages Command List \n")
    for command_name, command_type in registered_commands.items():
        print(f"{command_name}        {command_type.describe()}")
    print("u_packages v1.0.0")
    sys.exit(0)
