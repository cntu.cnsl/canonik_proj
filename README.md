# Upackage Project

### Another code/design training by copycat method

Study on how Python project are design and conducted in some of the big company.
This project will make a simple and custom code with intention to prove the information stated in the report

The target project is: [*ubuntu-package-status*](https://github.com/CanonicalLtd/ubuntu-package-status)

## SetupTools
The setuptools are setup for packaging purpose.
Tips are as follows:
* The use of find_packages is to include only the files that related to the distribute package (The unit tests, for example, does not need to include in the package)
* The requirements.txt can be used as an input of *install_requires*
* The content of README.md can become the long_description if it is not too long
* Entry Point allows the package to define a user-friendly name for installers of the package to execute. Installers like pip will create wrapper scripts to execute a function. In this project, after the user execute *u_packages* command, the main function in the file *app.py* is executed.
* Noted that the sdist (source distribution) in setuptools puts a minimal default set into the source distribution (such as py_modules/packages for Python, ext_modules/libraries for C). Sometimes this is enough, but usually you will want to specify additional files to distribute. The typical way to do this is to write a manifest template, called *MANIFEST.in* by default.
* More information on using setuptools with requirements: [setup.py or requirements.txt? Both!](https://jaenis.ch/blog/2018/setup-py-or-requirements-txt-both/)

## Enhanced Points
* Follows the design of Skynet presentation [Skynet](https://www.youtube.com/watch?v=0W0k6zP_Lto) to  allows a new packages to add new command to the u_packages by providing the similar entry_points without breaking the current design.
* This code uses 2 solutions for handling packaging: 1) **setuptools** that similar to the copied project and 2) **poetry** a new solution for handling the project.
* Added the .editorconfig, mypy and .flake8 configuration (learned from the 1st CodeReports - Github DAST)

## Testing
Current test available:
* poetry run flake8 (check PEP8 style - Linter)
* poetry run pytest (run unit test)

## Building and Running on Local
* poetry install
* poetry shell
* u-packages <params>
